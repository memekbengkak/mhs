//button input data mhs
public class Input_data_mhs extends javax.swing.JFrame {
    static String nama;
    public static String kategori = null;
    public static int absen;
    public static int uts;
    public static int uas;
    public static int tugas;
    public static int total_nilai;
    user_main_frame mf = new user_main_frame(main_frame.jumlah);
	
private void btn_submitActionPerformed(java.awt.event.ActionEvent evt) {                                           
        for(int i = 0; i < main_frame.jumlah; i++){
            model_siswa ms = new model_siswa(nama = tx_nama.getText(),
                                             absen = Integer.parseInt(tx_absen.getText()),
                                             tugas = Integer.parseInt(tx_tugas.getText()),
                                             uts = Integer.parseInt(tx_uts.getText()),
                                             uas = Integer.parseInt(tx_uas.getText()),
                                             kategori = null);
            
            
            total_nilai = (((absen * 14) / 100) + ((tugas * 20) / 100) + ((uts * 30) / 100) + ((uas * 40) / 100));
            if (total_nilai >= 80 && total_nilai <= 100) {
                kategori = "A";
            } else if (total_nilai >= 68 && total_nilai <= 79) {
                kategori = "B";
            } else if (total_nilai >= 56 && total_nilai <= 67) {
                kategori = "C";
            } else if (total_nilai >= 45 && total_nilai <= 55) {
                kategori = "D";
            } else if (total_nilai >= 0 && total_nilai <= 44) {
                kategori = "E";
            }
            
            //add data to array
            ms.setNama(nama);
            ms.setAbsen(absen);
            ms.setUts(uts);
            ms.setUas(uas);
            ms.setTugas(tugas);
            ms.setKategori(kategori);

            mf.save(ms);
            
            String all = ""+ms.getNama()+"\n"+ms.getAbsen()+"\n"+ms.getUts()+"\n"+ms.getUas()+"\n"+ms.getTugas()+"\n"+ms.getKategori();
            JOptionPane.showMessageDialog(null, ""+all);
        }
        dispose();
    }    
    
//END


//table display 
public class display_data_table extends javax.swing.JFrame {
    private model_siswa[] arr_emp;
    public String nama;
    static String kategori = null;
    public int absen;
    public int uts;
    public int uas;
    public int tugas;
    public int total_nilai;
	
public display_data_table() {
        initComponents();
        addRow();
    }
    
    public ArrayList ListUsers(){
        ArrayList<model_siswa> arr= new ArrayList<model_siswa>();
        model_siswa ui = new model_siswa(Input_data_mhs.nama,
                                         Input_data_mhs.absen,
                                         Input_data_mhs.uts,
                                         Input_data_mhs.uas,
                                         Input_data_mhs.tugas,
                                         Input_data_mhs.kategori);
        arr.add(ui);
        return arr;
    }
    public void addRow(){
        DefaultTableModel model = (DefaultTableModel) table_siswa.getModel();
        ArrayList<model_siswa> list = ListUsers();
        Object rowData[] = new Object[6];
        
        for(int i = 0; i < list.size(); i++){
            rowData[0] = list.get(i).nama;
            rowData[1] = list.get(i).absen;
            rowData[2] = list.get(i).uts;
            rowData[3] = list.get(i).uas;
            rowData[4] = list.get(i).tugas;
            rowData[5] = list.get(i).kategori;
            model.addRow(rowData);
            table_siswa.setAutoCreateColumnsFromModel(true);
            
        }
    }
    
//END


//model constructor
public class model_siswa {
    public String nama, kategori;
    public int absen;
    public int uts;
    public int uas;
    public int tugas;

    public model_siswa(String nama, int absen, int uts, int uas, int tugas, String kategori) {
        this.nama = nama;
        this.absen = absen;
        this.uts = uts;
        this.uas = uas;
        this.tugas = tugas;
        this.kategori = kategori;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public int getAbsen() {
        return absen;
    }

    public void setAbsen(int absen) {
        this.absen = absen;
    }

    public int getUts() {
        return uts;
    }

    public void setUts(int uts) {
        this.uts = uts;
    }

    public int getUas() {
        return uas;
    }

    public void setUas(int uas) {
        this.uas = uas;
    }

    public int getTugas() {
        return tugas;
    }

    public void setTugas(int tugas) {
        this.tugas = tugas;
    }
}

//END



//main frame (homepage)
public class main_frame extends javax.swing.JFrame {

    //user variable
    ArrayList<user_main_frame> userArrayList = new ArrayList<user_main_frame>();
    static private String nama;
    static private String strAbsen;
    static private String strUts;
    static private String strUas;
    static private String strTugas;
    String kategori = null;
    private int absen;
    private int uts;
    private int uas;
    private int tugas;
    public static int jumlah;
    public int total_nilai;
	
//submit banyak siswa
private void btn_submitActionPerformed(java.awt.event.ActionEvent evt) {                                           
        String j = JOptionPane.showInputDialog("input berapa siswa :");
        jumlah = Integer.parseInt(j);

        for (int i = 0; i <= jumlah - 1; i++) {
            Input_data_mhs utama = new Input_data_mhs();
            utama.setVisible(true);
        }
}

//END

//user_main_frame (nampung data ke array)
public class user_main_frame {
    private model_siswa[] arr_emp;
    private int i, jumlah;
    
    public user_main_frame(int pjg_array){
        arr_emp = new model_siswa[pjg_array];
    }
    
    public void save(model_siswa ms){
        arr_emp[i++] = ms;
    }
    public void viewAll() {
        display_data_table kedua = new display_data_table();
        
        for (int i = 0; i <= arr_emp.length; i++) {
            if (arr_emp[i] != null) {
                 display_data_table dis = new display_data_table();
                 DefaultTableModel datasiswa = (DefaultTableModel) dis.table_siswa.getModel();
                 List data = new ArrayList<>();
                 data.add(arr_emp[i].getNama());
                 data.add(arr_emp[i].getAbsen());
                 data.add(arr_emp[i].getUts());
                 data.add(arr_emp[i].getUas());
                 data.add(arr_emp[i].getTugas());
                 data.add(arr_emp[i].getKategori());

                 datasiswa.addRow(data.toArray());
            }
        }
        kedua.setVisible(true);
    }
	public void input(int jumlah){
        String j = JOptionPane.showInputDialog("input berapa siswa :");
        jumlah = Integer.parseInt(j);

        for (int i = 0; i <= jumlah - 1; i++) {
            Input_data_mhs utama = new Input_data_mhs();
            utama.setVisible(true);
        }
    }
}